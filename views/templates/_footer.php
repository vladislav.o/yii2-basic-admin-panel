<div id="bottom">

    <!-- Page footer -->
    <footer class="page-footer">
        <div class="container">
            <div class="d-sm-flex justify-content-sm-between align-items-sm-center">
                <div class="pf-item p-y-15 p-md-y-20 m-sm-r-15">© <?=Date('Y')?> <?= str_replace(['http://', 'https://'], '', Yii::$app->request->hostInfo) ?></div>
                <div class="pf-item p-y-15 p-md-y-20 flex-static"><a href="https://red-promo.ru" target="_blank" class="link link-f">Создание сайта</a> - Red Promo</div>
            </div>
        </div>
    </footer>

</div>
