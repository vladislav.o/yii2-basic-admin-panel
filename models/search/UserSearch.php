<?php

namespace app\models\search;

use app\models\User;
use yii\data\ActiveDataProvider;

/**
 * UserSearch
 */
class UserSearch extends User
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'username',
                    'password_hash',
                    'password_reset_token',
                    'email',
                    'auth_key',
                    'status',
                    'role',
                    'created_at',
                    'updated_at',
                    'password',
                ], 'safe'
            ],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        foreach ($this->attributes as $attribute => $val) {
            $query->andFilterWhere(['like', $attribute, $this->$attribute]);
        }
        //    ->andFilterWhere(['like', 'phone', $this->phone])
        //    ->andFilterWhere(['>=', 'created_at', $this->date_from ? strtotime($this->date_from . ' 00:00:00') : null])
        //    ->andFilterWhere(['<=', 'created_at', $this->date_to ? strtotime($this->date_to . ' 23:59:59') : null]);

        return $dataProvider;
    }
}
