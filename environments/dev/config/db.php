<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host='. getenv('DB_HOST') .';port=' . getenv('DB_PORT') . ';dbname=' . getenv('DB_NAME'),
    'username' => getenv('DB_USERNAME'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 3600, //1 hour
    //'schemaCache' => 'cache',
    'tablePrefix' => getenv('DB_PREFIX'),
];
