<?php
declare(strict_types=1);

use yii\bootstrap\BootstrapAsset;
use yii\i18n\PhpMessageSource;
use yii\rbac\PhpManager;
use yii\log\DbTarget;
use yii\swiftmailer\Mailer;
use app\backend\AdminModule;
use app\models\User;
use yii\caching\FileCache;
use yii\gii\Module as GiiModule;
use yii\debug\Module as DebugModule;
use yii2mod\settings\components\Settings;
use yii2mod\settings\Module as SettingModule;
use kartik\grid\Module as KartikGridViewModule;
use yii\gii\generators\crud\Generator as YiiGenerator;

$s = DIRECTORY_SEPARATOR;
$params = require __DIR__ . '/params.php'; // NOSONAR
$db = require __DIR__ . '/db.php'; // NOSONAR

$config = [
    'id' => 'basic',
    'name' => 'Admin panel',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@backend' => '@app/backend',
    ],
    'modules' => [
        'admin' => [
            'class' => AdminModule::class,
        ],
        'settings' => [
            'class' => SettingModule::class,
        ],
        'gridview' => [
            'class' => KartikGridViewModule::class
        ]
    ],
    'language' => 'ru-RU',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'xabXDg7_yrIrbPr6FN3xi_HixUa75SQd',
            'baseUrl' => '',
        ],
        'cache' => [
            'class' => FileCache::class,
        ],
        'user' => [
            'identityClass' => User::class,
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => static function () {
            return Yii::createObject([
                'class' => Mailer::class,
                'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'host' => Yii::$app->settings->get('SiteSettings', 'smtpHost'),
                    'port' => Yii::$app->settings->get('SiteSettings', 'smtpPort'),
                    'username' => Yii::$app->settings->get('SiteSettings', 'smtpUsername'),
                    'password' => Yii::$app->settings->get('SiteSettings', 'smtpPassword'),
                    'encryption' => Yii::$app->settings->get('SiteSettings', 'smtpSecure'),
                ],
            ]);
        },
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => DbTarget::class,
                    //'categories' => ['app\models\*', 'app\backend\*'],
                    'levels' => ['error', 'warning'],
                    'logVars' => [],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => file_exists(__DIR__ . $s . 'rewrite.php') ? require(__DIR__ . $s . 'rewrite.php') : [], // NOSONAR
        ],
        'authManager' => [
            'class' => PhpManager::class,
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => PhpMessageSource::class,
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
                'yii2mod.settings' => [
                    'class' => PhpMessageSource::class,
                    'basePath' => '@yii2mod/settings/messages',
                ],
            ],
        ],
        'settings' => [
            'class' => Settings::class,
        ],
        'formatter' => [
            'locale' => 'ru-RU',
            'dateFormat' => 'dd.MM.yyyy',
            'datetimeFormat' => 'dd.MM.yyyy HH:mm:ss',
            'booleanFormat' => ['Нет', 'Да'],
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'EUR',
            'nullDisplay' => '&nbsp;',
        ],
        'assetManager' => [
            'bundles' => [
                BootstrapAsset::class => [
                    'css' => [],
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => DebugModule::class,
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => GiiModule::class,
        'allowedIPs' => ['*'],
        'generators' => [ //here
            'crud' => [
                'class' => YiiGenerator::class, // generator class
                'templates' => [
                    'admin' => '@app/gii/admin', // template name => path to template
                ]
            ]
        ],
    ];
}

return $config;
