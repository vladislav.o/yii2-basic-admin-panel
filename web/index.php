<?php
use Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv::createUnsafeImmutable(__DIR__ . '/..');
$dotenv->load();

$debug = getenv('YII_DEBUG') === 'true';
$env = getenv('YII_ENV') === 'dev' ? 'dev' : 'prod';

define('YII_DEBUG', $debug);
define('YII_ENV', $env);

require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
