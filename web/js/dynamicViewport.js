var viewport = document.querySelector("meta[name=viewport]");
if (viewport) {
  document.head.removeChild(viewport);
}
var sibling = document.getElementById("sibling");
var dvp = document.createElement("meta");
dvp.setAttribute("name", "viewport");
if (screen.width <= 575) {
  dvp.setAttribute('content', 'width=375, user-scalable=no');
} else if (screen.width >= 1200) {
  dvp.setAttribute('content', 'width=1200, initial-scale=1');
} else {
  dvp.setAttribute('content', 'width=1200');
};
sibling.parentNode.insertBefore(dvp, sibling.nextSibling);
window.onresize = function () {
  if (screen.width <= 575 && dvp.getAttribute('content')!='width=375, user-scalable=no') {
    sibling.parentNode.removeChild(dvp);
    dvp.setAttribute('content', 'width=375, user-scalable=no');
    sibling.parentNode.insertBefore(dvp, sibling.nextSibling);
  } else if (screen.width >= 1200 && dvp.getAttribute('content')!='width=1200, initial-scale=1') {
    sibling.parentNode.removeChild(dvp);
    dvp.setAttribute('content', 'width=1200, initial-scale=1');
    sibling.parentNode.insertBefore(dvp, sibling.nextSibling);
  } else if (screen.width >= 576 && screen.width <= 1199 && dvp.getAttribute('content')!='width=1200')  {
    sibling.parentNode.removeChild(dvp);
    dvp.setAttribute('content', 'width=1200');
    sibling.parentNode.insertBefore(dvp, sibling.nextSibling);
  };
};