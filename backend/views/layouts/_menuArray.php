<?php

$controllerId = Yii::$app->controller->id;
$methodId = Yii::$app->controller->action->id;

$items = [];
$items = [
    [
        'label' => 'Пользователи',
        'url' => '/admin/user',
        'active' => ($controllerId == 'user')
    ],
];


return $items;