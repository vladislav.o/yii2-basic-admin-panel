<?php
use yii\widgets\Menu;

$controllerId = Yii::$app->controller->id;
$method = Yii::$app->controller->action->id;
?>


<div class="site-sidebar custom-scroll custom-scroll-dark">
    <?php

    $items = require(__DIR__ . '/_menuArray.php');

    foreach ($items as &$item) {
        if (isset($item['items'])) {
            $item['options'] = ['class' => 'with-sub'];
            $item['template'] = '<span>{label}</span>';
            foreach ($item['items'] as $key => $item2) {
                if (is_string($item2)) unset($item['items'][$key]);
            }
        }
    }

    echo Menu::widget([
        'options' => ['class' => 'sidebar-menu'],
        'submenuTemplate' => '<ul>{items}</ul>' . "\n",
        'items' => $items,
    ]);
    ?>

</div>