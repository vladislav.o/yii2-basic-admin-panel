<?php
declare(strict_types=1);

use app\models\search\UserSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/** @var View $this */
/** @var UserSearch $searchModel */
/** @var ActiveDataProvider $dataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-b-15">
    <div class="row">
        <div class="col-md-6">
            <h3 class="font-weight-bold m-b-10 m-md-b-0"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="col-md-6 text-lg-right">
            <button class="btn btn-primary label-left toggle-search m-r-10 hidden-md hidden-lg" type="button">
                <span class="btn-label"><i class="ti-arrow-down"></i></span> <?= Yii::t('app', 'Search') ?>
            </button>

        </div>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}\n{pager}\n{summary}",
    'filterModel' => $searchModel,
    'showOnEmpty' => true,
    'columns' => array_keys($searchModel->attributes),
]); ?>
