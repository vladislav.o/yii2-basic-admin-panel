<?php

namespace app\controllers;
use Yii;
use app\backend\components\FrontController;

class SiteController extends FrontController
{
    public $layout = 'secondary';

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'main';
        return $this->render('index');
    }
    
    public function actionSuccess()
    {
        return $this->render('success');
    }
    
    public function actionError()
    {
        return $this->render('error');
    }

}
