install:
	docker-compose up -d --build

recreate:				## Пересобрать все контейнеры игнорируя кеш
	docker-compose up -d --force-recreate

run-php:				## Зайти в консоль контейнера PHP
	docker exec -it yii2basicproject-php sh
