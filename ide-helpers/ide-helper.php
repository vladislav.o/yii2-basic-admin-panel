<?php
declare(strict_types=1);

use yii\BaseYii;
use yii2mod\settings\components\Settings;

class Yii extends BaseYii
{
    /**
     * @var BaseApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;
}

/**
 * @property Settings $settings
 */
abstract class BaseApplication extends yii\base\Application
{
}

/**
 * Здесь указать свойства для IDE
 */
class WebApplication extends yii\web\Application
{
}


/**
 * Здесь указать свойства для IDE
 */
class ConsoleApplication extends yii\console\Application
{
}
